-- user table

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	datetime_posted DATETIME,
	PRIMARY KEY (id)
)

-- post table

CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
	author_id INT NOT NULL,
	title VARCHAR(100) NOT NULL,
	content VARCHAR(5000),
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_author_id 
	FOREIGN KEY (author_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
)

-- Posts_comment table

CREATE TABLE posts_comments(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	content VARCHAR(5000),
	datetime_commented DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_user_id 
	FOREIGN KEY (post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,

	CONSTRAINT fk_posts_user_id 
	FOREIGN KEY (user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
)


-- postlike table

CREATE TABLE posts_likes(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	datetime_commented DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_likes_post_id 
	FOREIGN KEY (post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,

	CONSTRAINT fk_posts_like_user_id 
	FOREIGN KEY (user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
)







